# An Application to Electrical Networks

### Example

Left node is $A$, right node is $B$.

_Junction Rule_:

$$
\begin{aligned}
A&:& I_2 + I_3 &= I_1 \\
B&:& I_1 &= I_2 + I_3 \\
\implies&& I_1 - I_2 - I_3 &= 0
\end{aligned}
$$

Top: $-20 = -6I_1 - 4I_2$

Bottom: $-10 = 4 I_2 - 2 I_3$

$$
\begin{aligned}
\left[
\begin{array}{ccc|c}
1 & -1 & -1 & 0 \\
-6 & -4 & 0 & -20 \\
0 & 4 & -2 & -10
\end{array}
\right]
\end{aligned}
$$

Operations:

- $6r_1 + r_2$
- $\frac1{10} r_2$
- $r_2 + r_1$ and $-4r_2 + r_3$
- $-\frac5{22} r_3$
- $-\frac35 r_3 + r_2$ and $\frac25 r_3 + r_1$

$$
\begin{aligned}
\left[
\begin{array}{ccc|c}
1 & 0 & 0 & \frac{40}{11} \\
0 & 1 & 0 & \frac{3}{11} \\
0 & 0 & 1 & \frac{45}{11}
\end{array}
\right]
\end{aligned}
$$

$$
\begin{aligned}
I_1 &= \frac{40}{11} \\
I_2 &= -\frac{5}{11} \\
I_3 &= \frac{45}{11}
\end{aligned}
$$

### Practice (TODO)