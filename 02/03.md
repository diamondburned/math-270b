# Matrix Multiplication

An $m \times n \ A$ matrix multiplied by $n \times p \ B$ matrix. 
Multiply $A$'s row by $B$'s column.