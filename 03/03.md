# Eigenvalues

___Example___: $A = \left[
\begin{array}{}
4 & -2 \\ -1 & 3
\end{array}
\right]$

___Solution___:

$$
\begin{aligned}
&\lambda I - A \\
= &\left[
\begin{array}{}
\lambda & 0 \\ 0 & \lambda
\end{array}
\right] - \left[
\begin{array}{}
4 & -2 \\ -1 & 3
\end{array}
\right]
\end{aligned}
$$

$$
\begin{aligned}
C_A  (\lambda) &= \det(\lambda I - A) \\
&= \det \left[
\begin{array}{}
\lambda - 4 & 2 \\ 1 & \lambda - 3
\end{array}
\right] \\
&= (\lambda -4)(\lambda -3) - 2 \\
&= \lambda^2 - 7 \lambda + 12 - 2 \\
&= \lambda^2 - 7 \lambda + 10 \\
\end{aligned}
$$

---

___Example___: $A = \left[
\begin{array}{}
3 & -4 & 2 \\ 1 & -2 & 2 \\ 1 & -5 & 5
\end{array}
\right]$

___Solution___:

$$
\begin{aligned}
C_A(\lambda) &= \det (\lambda I - A) \\
&= \det \left[
\begin{array}{}
\lambda - 3 & 4 & -2 \\
-1 & \lambda + 2 & -2 \\
-1 & 5 & \lambda - 5
\end{array}
\right] \\
&= (\lambda - 3)(-1)^{3+3} \left[
\begin{array}{}
\lambda - 3 & 2 \\ -1 & \lambda
\end{array}
\right] \\
&= (\lambda - 3)(\lambda - 2)(\lambda - 1) \\
\implies &\begin{cases}
\lambda_1 = 3 \\
\lambda_2 = 2 \\
\lambda_3 = 1
\end{cases}
\end{aligned}
$$

<br>

For $\lambda_1 = 3$, $(3I - A)\vec x = \vec 0$

$$
\begin{aligned}
&\left[
\begin{array}{ccc|c}
0 & 4 & -2 & 0 \\
-1 & 5 & -2 & 0 \\
-1 & 5 & -2 & 0
\end{array}
\right] \\
\sim &\left[
\begin{array}{ccc|c}
1 & 0 & -\frac12 & 0 \\
0 & 1 & -\frac12 & 0 \\
0 & 0 & 0 & 0
\end{array}
\right]
\end{aligned}
$$

$$
\begin{aligned}
x_1 &= \frac12 t \\
x_2 &= \frac12 t \\
x_3 &= t
\end{aligned}
\ \ \ \ \
\begin{aligned}
\vec x = t \left[
\begin{array}{}
\frac12 \\ \frac12 \\ 1
\end{array}
\right]
\end{aligned}
\ \ \
\begin{aligned}
\vec x_1 = \left[
\begin{array}{}
1 \\ 1 \\ 2
\end{array}
\right]
\end{aligned}
$$

(Let $t = 2$)

<br>

For $\lambda_2 = 2$, $(2I - A)\vec x = \vec 0$

$$
\begin{aligned}
&\left[
\begin{array}{ccc|c}
-1 & 4 & -2 & 0 \\
-1 & 4 & -2 & 0 \\
-1 & 5 & -3 & 0
\end{array}
\right] \\
\sim &\left[
\begin{array}{ccc|c}
1 & 0 & -2 & 0 \\
0 & 1 & -1 & 0 \\
0 & 0 & 0 & 0
\end{array}
\right]
\end{aligned}
$$

$$
\begin{aligned}
x_1 &= 2 t \\
x_2 &= t \\
x_3 &= t
\end{aligned}
\ \ \ \ \
\begin{aligned}
\vec x = t \left[
\begin{array}{}
2 \\ 1 \\ 1
\end{array}
\right]
\end{aligned}
\ \ \
\begin{aligned}
\vec x_2 = \left[
\begin{array}{}
2 \\ 1 \\ 1
\end{array}
\right]
\end{aligned}
$$

(Let $t = 1$)

<br>

For $\lambda_3 = 1$, $(I - A)\vec x = \vec 0$

$$
\begin{aligned}
&\left[
\begin{array}{ccc|c}
-2 & 4 & -2 & 0 \\
-1 & 3 & -2 & 0 \\
-1 & 5 & -4 & 0
\end{array}
\right] \\
\sim &\left[
\begin{array}{ccc|c}
1 & 0 & -1 & 0 \\
0 & 1 & -1 & 0 \\
0 & 0 & 0 & 0
\end{array}
\right]
\end{aligned}
$$

$$
\begin{aligned}
x_1 &= t \\
x_2 &= t \\
x_3 &= t
\end{aligned}
\ \ \ \ \
\begin{aligned}
\vec x = t \left[
\begin{array}{}
1 \\ 1 \\ 1
\end{array}
\right]
\end{aligned}
\ \ \
\begin{aligned}
\vec x_3 = \left[
\begin{array}{}
1 \\ 1 \\ 1
\end{array}
\right]
\end{aligned}
$$

(Let $t = 1$)

<br>

$$
\begin{aligned}
P &= \left[
\begin{array}{}
1 & 2 & 1 \\ 1 & 1 & 1 \\ 2 & 1 & 1
\end{array}
\right] \\
P^{-1}  AP &= \text{diag} (3, 2, 1) = \left[
\begin{array}{}
3 & 0 & 0 \\ 0 & 2 & 0 \\ 0 & 0 & 1
\end{array}
\right]
\end{aligned}
$$

---

___Example___: $A = \left[
\begin{array}{}
1 & 0 & 1 \\ 0 & 1 & 0 \\ 0 & 0 & -3
\end{array}
\right]$

$$
\begin{aligned}
C_A (\lambda) &= \det(\lambda I - A) \\
&= \det \left[
\begin{array}{}
\lambda-1 & 0 & -1 \\
0 & \lambda-1 & 0 \\
0 & 0 & \lambda+3
\end{array}
\right] \\
&= (\lambda-1) (\lambda-1) (\lambda+3)
\end{aligned}
$$

$$
\begin{aligned}
\lambda_1 = 1 \text{ mult } 2, \ \lambda_2 = -3
\end{aligned}
$$

For $\lambda_1 = 1,$ $(I-A)\vec x = \vec 0$

$$
\begin{aligned}
&\left[
\begin{array}{ccc|c}
0 & 0 & -1 & 0 \\
0 & 0 & 0 & 0 \\
0 & 0 & 4 & 0
\end{array}
\right]
\sim &\left[
\begin{array}{ccc|c }
0 & 0 & 1 & 0 \\
0 & 0 & 0 & 0 \\
0 & 0 & 0 & 0 \\
\end{array}
\right]
\end{aligned}
$$

$$
\begin{aligned}
x_1 &= s \\
x_2 &= t \\
x_3 &= 0
\end{aligned}
$$

$$
\begin{aligned}
\vec x &= s \left[
\begin{array}{}
1 \\ 0 \\ 0
\end{array}
\right]
+ t \left[
\begin{array}{}
0 \\ 1 \\ 0
\end{array}
\right]
\end{aligned}
$$

$$
\begin{aligned}
\vec x_1 = \left[
\begin{array}{}
1 \\ 0 \\ 0
\end{array}
\right], \
\vec x_2 =\left[
\begin{array}{}
0 \\ 1 \\ 0
\end{array}
\right] 
\end{aligned}
$$

For $\lambda_2 = 3$

$$
\begin{aligned}
&\left[
\begin{array}{}
-4 & 0 & -1 & 0 \\
0 & -4 & 0 & 0 \\
0 & 0 & 0 & 0
\end{array}
\right] \\
\sim &\left[
\begin{array}{}
1 & 0 & \frac14 & 0 \\
0 & 1 & 0 & 0 \\
0 & 0 & 0 & 0
\end{array}
\right]
\end{aligned}
$$

$$
\begin{aligned}
x_1 &= \frac14 t \\
x_2 &= AAAAAAAAAAAAAAAAAAAAAAAAAAAA
\end{aligned}
$$

---

___Example___: $A = \left[
\begin{array}{}
1 & 1 & 0 \\ 0 & 1 & 0 \\ 0 & 0 & 2
\end{array}
\right]$

___Solution___:

$$
\begin{aligned}
\vec x_1 = \left[
\begin{array}{}
1 \\ 0 \\ 0
\end{array}
\right]
\end{aligned}
$$

Since $\lambda_1 = 1$ has mult 2 but only produces 1 eigenvector, $A$
is not diagonalizable.