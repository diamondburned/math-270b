# Subspaces and Spans

1. The zero vector of $R^n$, $0_n$, is in $U$
2. $U$ is closed under addition, i.e., for all $u, w ∈ U$, $u + w ∈ U$
3. $U$ is closed under scalar multiplication, i.e., for all $u ∈ U$ and $k ∈ R$, $ku ∈ U$.

---

___Example___:

$\vec 0$-check:

$
\left[
\begin{array}{}
0 \\ 0 \\ 0
\end{array}
\right] \in L$ since $0 \vec d$.

<br>

Cl-$+$: Suppose $\vec u, \vec v \in L$, then $\vec u = t_1 \vec d$
and $\vec v = t_2 \vec d$ where $t_1, t_2 \in \R$.

Now, $\vec u + \vec v = t_1 \vec d + t_2 \vec d = (t_1 + t_2) \vec d = t_3 \vec d$ where $t_3 \in \R$.

<br>

Cl-$\cdot$: Let $\vec u \in L$ and $k \in R$. Then, $\vec u = t \vec d$ where $t \in R$.

Now, $k \vec u = k(t\vec d) = (kt)\vec d = t_1 \vec d$ where $t_1 \in \R$.

<br>

Therefore $L$ is a subspace of $\R^3$.

---

___Example___:

$\vec 0$-check: $\left[
\begin{array}{}
0 \\ 0 \\ 0
\end{array}
\right] \in M$ since $\vec n \cdot \vec 0 = 0$.

<br>

Cl-$+$: Let $\vec u, \vec v \in M$. Then, $\vec n \cdot \vec u = 0$ and $\vec n \cdot \vec v = 0$.

Now, $\vec n \cdot (\vec u + \vec v) = \vec n \cdot \vec u + \vec n \cdot \vec v = 0+0 = 0$. So $\vec u + \vec v \in M$.

<br>

Cl-$\cdot$: Let $\u \in M$ and $k \in \R$. Then, $\vec n \cdot \vec u = 0$.

Now, $\vec n \cdot(k \cdot \vec u) = k (\vec n \cdot \vec u) = 0k = 0$. So $k \vec u \in M$.

<br>

Therefore $M$ is a subspace of $\R^3$.

---

___Example___:

$\vec 0$-check: $\left[
\begin{array}{}
0 \\ 0 \\ 0 \\ 0
\end{array}
\right] \in U$ since $0 \in \R$ and $2(0) - 0 = 0 + 2(0)$.

<br>

Cl-$+$: Let $\vec u, \vec v \in U$. Then, $\vec u = \left[
\begin{array}{}
a_1 \\ b_1 \\ c_1 \\ d_1
\end{array}
\right]$ where $a_1, b_1, c_1, c_1 \in \R$ and $2a_1 - b_1 = c_1 + 2d_1$
and $\vec v = \left[
\begin{array}{}
a_2 \\ b_2 \\ c_2 \\ d_2
\end{array}
\right]$ where $a_2, b_2, c_2, d_2 \in \R$ and $2 a_2 - b_2 = c_2 + 2d_2$.

<br>

Now, $\vec u + \vec v = \left[
\begin{array}{}
a_1 + a_2 \\ b_1 + b_2 \\ c_1 + c_2 \\ d_1 + d_2
\end{array}
\right]$ where $a_1+a_2, b_1 + b_2, c_1 + c_2, d_1 + d_2 \in \R$, and

$$
\begin{aligned}
&2(a_1 + a_2) - (b_1 + b_2) \\
= \ &2a_1 + 2a_2 - b_1 - b_2 \\
= \ &2a_1 -b_1 + 2a_2 - b_2 \\
= \ &c_1 + 2d_1 + c_2 2d_2
\end{aligned}
$$

So $\vec u + \vec v = U$.

<br>

Cl-$\cdot$: Let $\vec u \in U$ and $k \in \R$. Then, $\vec u = \left[
\begin{array}{}
a \\ b \\ c \\ d
\end{array}
\right]$ where $a, b, c, d \in \R$ and $2a - b = c + 2d$.

Now, $k \vec u = \left[
\begin{array}{}
ka \\ kb \\ kc \\ kd
\end{array}
\right]$ where $ka, kb, kc, kd \in \R$ and $2(ka) - (kb) = kc + 2(kd)$.

So $k \vec u \in U$.

Therefore, $U$ is a subspace of $\R^3$.

---

$$
\begin{aligned}
\nullspace(A) &= \{x \in \R^n | A\vec x = \vec 0_m\} \\
\im(A) = \text{Rng}(A) &= \{Ax | c \in \R^n\}
\end{aligned}
$$

---

___Example___: Let $\vec x, \vec y, \vec z$. Is $x \in \span\{\vec y, \vec z\}$?

___Solution___: Are there $c_1, c_2 \in \R$ such that $c_1 \vec y + c_2 \vec z = \vec x$?

$$
\begin{aligned}
c_1 \left[
\begin{array}{}
2 \\ 1 \\ -3 \\ 5
\end{array}
\right] + c_2 \left[
\begin{array}{}
-1 \\ 0 \\ 2 \\ -3
\end{array}
\right] = \left[
\begin{array}{}
8 \\ 3 \\ -13 \\ 20
\end{array}
\right]
\end{aligned}
$$

$$
\begin{aligned}
\left[
\begin{array}{cc|c}
2 & -1 & 8 \\
1 & 0 & 3 \\
-3 & 2 & -13 \\
5 & -3 & 20
\end{array}
\right] \sim \left[
\begin{array}{cc|c}
1 & 0 & 3 \\
0 & 1 & -2 \\
0 & 0 & 0 \\
0 & 0 & -1
\end{array}
\right]
\end{aligned}
$$

There is no solution, so this is inconsistent.
Therefore, $x \not\in \span\{\vec y, \vec z\}$.

---

$$
\begin{aligned}
\left[
\begin{array}{cc|c}
2 & -1 & 8 \\
1 & 0 & 3 \\
-3 & 2 & -13 \\
5 & -3 & 21
\end{array}
\right] \sim \left[
\begin{array}{cc|c}
1 & 0 & 3 \\
0 & 1 & -2 \\
0 & 0 & 0 \\
0 & 0 & 0
\end{array}
\right]
\end{aligned}
$$

$3 \vec y - 2 \vec z = \vec w$, so $\vec w \in \span\{\vec y, \vec z\}$.

---

___Example___: Since $2a - b = c + 2d$, then $2a - b - 2d = c$.

Thus,

$$
\begin{aligned}
U = &\ \left\{ \left[
\begin{array}{}
a \\ b \\ 2a-b-2d \\ d
\end{array}
\right]: a, b, d \in \R \right\} \\
= &\ \left\{ a \left[
\begin{array}{}
1 \\ 0 \\ 2 \\ 0
\end{array}
\right] + b \left[
\begin{array}{}
0 \\ 1 \\ -1 \\ 0
\end{array}
\right] + d \left[
\begin{array}{}
0 \\ 0 \\ -2 \\ 1
\end{array}
\right]: a, b, d \in \R \right\} \\
\ &\ \span \left\{ \left[
\begin{array}{}
1 \\ 0 \\ 2 \\ 0
\end{array}
\right], \left[
\begin{array}{}
0 \\ 1 \\ -1 \\ 0
\end{array}
\right], \left[
\begin{array}{}
0 \\ 0 \\ -2 \\ 1
\end{array}
\right] \right\}
\end{aligned}
$$

---

___Example___: Prove $U_1 = \span\{\vec x, \vec y\} = U_2 = \span\{2 \vec x - \vec y, 2 \vec y + \vec x\}$.

___Solution___:

Show that $U_2 \subseteq U_1$ and $U_1 \subseteq U_2$.

Since $U_1$ and $U_2$ are spanning sets, they're subspaces of $\R^n$.

$U_2 \subseteq U_1$: Clearly, $2 \vec x - \vec y, \ 2 \vec y + \vec x \in U_1$.

$U_1 \subseteq U_2$: We need $\vec x = c_1 (2\vec x - \vec y) + c_2 (2 \vec y + \vec x)$
and $\vec y = c_1 (2 \vec x - \vec y) + c_2 (2 \vec y + \vec x)$.

$$
\begin{aligned}
\vec x &= 2c_1 \vec x - c_1 \vec y + 2 c_2 \vec y + c_2 \vec x \\
&= (2c_1 + c_2) \vec x + (-c_2 + 2c_2) \vec y \\
\end{aligned}
$$

$$
\begin{aligned}
\implies \begin{cases}
2c_1 + c_2 &= 1 \\
-c_1 + 2c_2 &= 0
\end{cases}
\implies \begin{cases}
c_1 &= \frac25 \\
c_2 &= \frac15 \\
\end{cases}
\end{aligned}
$$

$$
\begin{aligned}
\vec y &= 2c_1 \vec x - c_1 \vec y + 2 c_2 \vec y + c_2 \vec x \\
&= (2c_1 + c_2) \vec x + (-c_2 + 2c_2) \vec y \\
\end{aligned}
$$

$$
\begin{aligned}
\implies \begin{cases}
2c_1 + c_2 &= 0 \\
-c_1 + 2c_2 &= 1
\end{cases}
\implies \begin{cases}
c_1 &= -\frac15 \\
c_2 &= \frac25 \\
\end{cases}
\end{aligned}
$$

Thus, there exists $c_1$ and $c_2$ such that $\vec x, \vec y \in U_2$.

Therefore, $\span\{\vec x, \vec y\} = U_1 \subseteq U_2$.

So $U_1 = U_2$.