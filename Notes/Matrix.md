Solve the system

$$
\begin{array}{}
2x &+ &y &+ &3z &= &1 \\
x &+ &2y &- &z &= &0 \\
x &- &4y &+ &9z &= &2
\end{array}
$$

Solution:

$$
\left[
\begin{array}{ccc|c}
2 & 1 & 3 & 1 \\
1 & 2 & -1 & 0 \\
1 & -4 & 9 & 2 \\
\end{array}
\right]

\begin{array}{}
r_1 \leftrightarrow r_2 \\ \\ \\
\end{array}

\left[
\begin{array}{ccc|c}
1 & 2 & -1 & 0 \\
1 & 2 & -1 & 0 \\
1 & -4 & 9 & 2 \\
\end{array}
\right]

\\[1em]

\begin{array}{}
-2r_1 + r_2 \\
-r_1 + r_2 \\
\\
\end{array}

\left[
\begin{array}{ccc|c}
1 & 2 & -1 & 0 \\
0 & -3 & 5 & 1 \\
0 & -6 & 10 & 2
\end{array}
\right]

\\[1em]

\begin{array}{}
-2 r_2 + r_3 \\ \\ \\
\end{array}

\left[
\begin{array}{ccc|c}
1 & 2 & -1 & 0 \\
0 & -3 & 5 & 1 \\
0 & 0 & 0 & 0
\end{array}
\right]

\\[1em]

\begin{array}{}
-\frac13 r_2
\end{array}

\left[
\begin{array}{ccc|c}
1 & 2 & -1 & 0 \\
0 & 1 & -\frac53 & -\frac13 \\
0 & 0 & 0 & 0
\end{array}
\right]

\\[1em]

\begin{array}{}
-2 r_2 + r_1 \\ \\ \\
\end{array}

\left[
\begin{array}{ccc|c}
1 & 0 & \frac73 & \frac23 \\
0 & 1 & -\frac53 & -\frac13 \\
0 & 0 & 0 & 0
\end{array}
\right]
$$

---

Solve the system

$$
\begin{array}{}
x &+ &y &+ &2z &= -1 \\
2x &+ &y &+ &3z &= 0 \\
&- &2y &+ &z &= &2
\end{array}
$$

Solution:

$$
\begin{aligned}
&\left[
\begin{array}{ccc|c}
1 & 1 & 2 & -1 \\
2 & 1 & 3 & 0 \\
0 & -2 & 1 & 2
\end{array}
\right]

\\[1em]

\begin{array}{}
-2 r_1 + r_2 \\ \\ \\
\end{array}

&\left[
\begin{array}{ccc|c}
1 & 1 & 2 & -1 \\
0 & -1 & -1 & 2 \\
0 & -2 & 1 & 2
\end{array}
\right]

\\[1em]

\begin{array}{}
-r_2 \\ \\ \\
\end{array}

&\left[
\begin{array}{ccc|c}
1 & 1 & 2 & -1 \\
0 & 1 & 1 & -2 \\
0 & -2 & 1 & 2
\end{array}
\right]

\\[1em]

\begin{array}{}
-r_2 + 1 \\ 2r_2 + r_3 \\ \\
\end{array}

&\left[
\begin{array}{ccc|c}
1 & 0 & 1 & 1 \\
0 & 1 & 1 & -2 \\
0 & 0 & 3 & -2
\end{array}
\right]

\\[1em]

\begin{array}{}
\frac13 r_3 \\ \\ \\
\end{array}

&\left[
\begin{array}{ccc|c}
1 & 0 & 1 & 1 \\
0 & 1 & 1 & -2 \\
0 & 0 & 1 & -\frac23
\end{array}
\right]

\\[1em]

\begin{array}{}
-r_3 + r_2 \\ -r_3 + r_1 \\ \\
\end{array}

&\left[
\begin{array}{ccc|c}
1 & 0 & 1 & 1 \\
0 & 1 & 1 & -2 \\
0 & 0 & 1 & -\frac23
\end{array}
\right]
\end{aligned}
$$

---

![](1.png):w
