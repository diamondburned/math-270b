$$
\begin{array}{ccc|c}
1 & 2 & 1 & 2 \\
0 & 1 & -1 & -\frac52 \\
0 & 0 & 0 & 1
\end{array}
$$

![](Images/1.png)

## TODO

- Study variables, parameters, rank, etc.

---

1.3 Q2

![](Images/2.png)

Make a matrix like $[\vec a \ \vec b \ \vec c \ | \vec V_{1} \ \vec V_2 \ \vec V_3]$.

We can combine this because the steps to reduce to
row-echelon form is the same (lhs same).

$\vec V_3$ is not a L.C. (all 0).

$$
\begin{aligned}
C_1 &= -3 + 2t \\
C_2 &= -5 + 3t \\
C_3 &= t
\end{aligned}
$$

Lyryx doesn't take $t$, so we do:

$$
\begin{aligned}
(-3 + 2t) \vec a + (-5 + 3t) \vec b + (t) \vec c &= \vec V_1
\end{aligned}
$$

Pick any real number for $t$, e.g. $t = 0$:

$$
\begin{aligned}
-3\vec a - 4\vec b &= \vec V_1
\end{aligned}
$$

---

Quiz 2: 2.1 ~ 2.2

---

**Basic solutions** are the vector matrix part of the $a[] + b[]$ form.

---

Exam 1: 1.1 ~ 2.5

Study for Junction Rule.